package test.com.jms.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import java.awt.font.TextMeasurer;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jms.listener.ConsumerListener;

public class ConsumerListenerTest {
	
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{vendorName:\'Microsofttest3\',firstName:\'RinatList\',lastName:\'SmithTest\',address:\'123 Main test\',city:\'TulsaTest\',state:\'OKTest\',zip:\'71345Test\',email:\'Bob@microsoft.test\',phoneNumber:\'test-123-test\'}";

		
	@Before
	public void setUp() throws Exception {	
		
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		 
	}

	@Test
	public void testOnMessage() throws JMSException {
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
	//	assertNull(message);
		verify(message);
	}

}
