package test.com.jms.listener;

import static org.junit.Assert.assertNotNull;

import java.io.FileReader;
import java.net.UnknownHostException;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonParser;
import com.google.gson.JsonStreamParser;
import com.jms.adapter.ConsumerAdapter;

public class ConsumerAdapterTest {


	private String json = "{vendorName:\'Microsofttest2\',firstName:\'Rinat\',lastName:\'SmithTest\',address:\'123 Main test\',city:\'TulsaTest\',state:\'OKTest\',zip:\'71345Test\',email:\'Bob@microsoft.test\',phoneNumber:\'test-123-test\'}";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongo() throws UnknownHostException {
		ConsumerAdapter adapter = new ConsumerAdapter();

		
		adapter.sendToMongo(json);
		
		assertNotNull(json);

	}

}
